"use strict";
var $ = require("jquery");
var moment = require("moment");

var redate = exports.redate = function() {
  for(var d of $(".date")) {
    var $d = $(d);
    $d.html(moment($d.text()).local().format());
  }
};

