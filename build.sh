#!/usr/bin/env zsh
cargo build --release
rm -rf sitemap
mkdir sitemap
cd js
yarn
webpack-cli app.js -o ../static/js/dist/main.js
cd ..
PORT=5000 ./target/release/gblog &
PID=$!
sleep 1
cd sitemap
wget -m http://127.0.0.1:5000/
wget -m http://127.0.0.1:5000/static/
find 127.0.0.1:5000 | sed 's/127.0.0.1:5000/https:\/\/george-f-rogers.herokuapp.com/' | tee ~/gblog/static/sitemap.txt
echo $PID
kill $PID
wait $PID
cd ..
rm -rvf sitemap
