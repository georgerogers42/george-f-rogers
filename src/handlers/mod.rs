use std::io;
use std::sync::Arc;

use actix_web::{HttpRequest, HttpResponse, Path, App};
use actix_web::middleware::Logger;
use actix_web::fs::{StaticFiles, NamedFile};
use actix_web::http::{ContentEncoding, Method};

use askama::Template;

use models;

#[derive(Clone, PartialEq, Template)]
#[template(path="base.html")]
struct BaseTpl { }

#[derive(Clone, PartialEq, Template)]
#[template(path="index.html")]
struct ArticlesTpl<'a> { 
    _parent: BaseTpl,
    articles: &'a[Arc<models::Article>],
    pre: usize,
    cur: usize,
    nxt: usize,
}

#[derive(Clone, PartialEq, Template)]
#[template(path="all.html")]
struct AllTpl<'a> {
    _parent: BaseTpl,
    articles: &'a[Arc<models::Article>],
}

#[derive(Clone, PartialEq, Template)]
#[template(path="article.html")]
struct ArticleTpl<'a> {
    _parent: BaseTpl,
    article: &'a models::Article,
}

const PER_PAGE: usize = 2;

pub fn index(_req: HttpRequest) -> io::Result<HttpResponse> {
    let r = HttpResponse::Ok()
        .content_type("text/html")
        .content_encoding(ContentEncoding::from("UTF-8"))
        .body(AllTpl{ _parent: BaseTpl{}, articles: &models::ARTICLE_VEC }.render().unwrap());
    Ok(r)
}

pub fn article(p: Path<(String,)>) -> io::Result<HttpResponse> {
    let r = match models::ARTICLE_MAP.get(&p.0) {
        Some(article) => {
            HttpResponse::Ok()
                .content_type("text/html")
                .content_encoding(ContentEncoding::from("UTF-8"))
                .body(ArticleTpl{ _parent: BaseTpl{}, article: &article }.render().unwrap())
        } None => {
            HttpResponse::NotFound().finish()
        }
    };
    Ok(r)
}

fn render_page(p: usize) -> io::Result<HttpResponse> {
    let i = p * PER_PAGE;
    let l = models::ARTICLE_VEC.len();
    let articles = &models::ARTICLE_VEC[usize::min(l, i)..usize::min(l, i+PER_PAGE)];
    let r = HttpResponse::Ok()
        .content_type("text/html")
        .content_encoding(ContentEncoding::from("UTF-8"))
        .body(ArticlesTpl{ _parent: BaseTpl{}, articles: articles, pre: usize::max(1, p)-1, cur: p, nxt: usize::min(p+1, (l-1)/PER_PAGE) }.render().unwrap());
    Ok(r)
}

fn first(_req: HttpRequest) -> io::Result<HttpResponse> {
    render_page(0)
}

fn page(p: Path<(usize,)>) -> io::Result<HttpResponse> {
    render_page(p.0)
}

fn favicon(_req: HttpRequest) -> io::Result<NamedFile> {
    NamedFile::open("static/favicon.ico")
}

pub fn app() -> App {
    App::new()
        .middleware(Logger::default())
        .route("/", Method::GET, first)
        .route("/page/{idx}", Method::GET, page)
        .route("/all", Method::GET, index)
        .route("/article/{slug}", Method::GET, article)
        .route("/favicon.ico", Method::GET, favicon)
        .handler("/static/", StaticFiles::new("static").unwrap().show_files_listing())
}
