extern crate actix;
extern crate actix_web;
extern crate serde;
extern crate serde_json;
#[macro_use]
extern crate serde_derive;
#[macro_use]
extern crate askama;
extern crate glob;
#[macro_use]
extern crate lazy_static;
extern crate chrono;
extern crate pulldown_cmark;
#[macro_use]
extern crate log;
extern crate env_logger;

use std::env;
use actix_web::server;

mod handlers;
mod models;

fn main() {
    env_logger::init();
    let host = env::var("HOST").unwrap_or("0.0.0.0".to_owned());
    let port = env::var("PORT").unwrap_or("8000".to_owned());
    let on = format!("{}:{}", host, port);
    server::new(handlers::app).bind(on).unwrap().run();
}
