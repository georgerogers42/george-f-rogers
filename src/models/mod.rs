use std::sync::Arc;
use std::collections::HashMap;
use std::io;
use std::fs;
use std::io::prelude::*;
use std::error::Error;

use chrono::prelude::*;
use serde_json;
use glob::glob;
use pulldown_cmark::{html, Parser};

#[derive(Clone, PartialEq, Eq, Debug, Serialize, Deserialize)]
pub struct Metadata {
    pub title: String,
    pub slug: String,
    pub author: String,
    pub posted: DateTime<Utc>,
}

#[derive(Clone, PartialEq, Eq, Debug, Serialize, Deserialize)]
pub struct Article {
    pub metadata: Metadata,
    pub contents: String,
}

pub fn render_markdown(raw_contents: &str) -> String {
    let p = Parser::new(raw_contents);
    let mut contents = "".to_owned();
    html::push_html(&mut contents, p);
    contents
}

pub fn load_article<R: BufRead>(mut r: R) -> Result<Article, Box<Error>> {
    let mut metapara = "".to_owned();
    loop {
        let mut line = "".to_owned();
        r.read_line(&mut line)?;
        if line.trim() == "" {
            break;
        }
        metapara.push_str(&line);
    }
    let metadata = serde_json::from_str(&metapara)?;
    let mut raw_contents = "".to_owned();
    r.read_to_string(&mut raw_contents)?;
    let contents = render_markdown(&raw_contents);
    let r = Article { metadata, contents };
    Ok(r)
}

pub fn load_articles(gpat: &str) -> Result<Vec<Arc<Article>>, Box<Error>> {
    let mut articles = vec![];
    for fname in glob(gpat)? {
        let fname = fname?;
        let f = io::BufReader::new(fs::File::open(fname)?);
        articles.push(Arc::new(load_article(f)?));
    }
    articles.sort_by(|a, b| b.metadata.posted.cmp(&a.metadata.posted));
    Ok(articles)
}

pub fn build_article_map(articles: &[Arc<Article>]) -> HashMap<String, Arc<Article>> {
    let mut hm = HashMap::new();
    for article in articles {
        hm.insert(article.metadata.slug.clone(), article.clone());
    }
    hm
}

lazy_static! {
    pub static ref ARTICLE_VEC: Vec<Arc<Article>> = load_articles("articles/*.md").unwrap();
    pub static ref ARTICLE_MAP: HashMap<String, Arc<Article>> = build_article_map(&ARTICLE_VEC);
}
