{ "title": "Rust And The Generations Of Programming Languages",
  "author": "George Rogers",
  "posted": "2018-07-16T13:00:00-0500",
  "slug": "why-rust" }
  
To understand why Rust matters we must go through the generations of programming languages.

1. Machine Language &ndash; A low level tool that you wrote the instructions in a hex editor.
2. Assembler &ndash; A higher level tool that allowed you to write in machine mnemonics, and have names for linkage.
3. First Generation High Level Languages &ndash; For example: C, C++, Fortran, Cobol, Ada, and others. These languages do not have garbage collection. These languages freed the programmer from the machine instructions and calling conventions, and allowed expressions to be used for math.
4. Second Generation High Level Languages &ndash; For example: Java, JavaScript, Perl, Python, Ruby, C#, VB, VB.net, Haskell, ML, and others. These languages freed the programmer from Memory Management using Garbage Collection, and many allowed Lambda The Ultimate to be used.
5. Third Generation High Level Languages such as: Rust, and various research languages. These languages combine the pros of the previous two language generations, by using the type system to enable safe manual memory management, so that the programmer is freed but still in control of memory management.

Now the benefits of widespread use of 5th generation programming languages are:

1. Higher productivity of low level programming tasks via increasing the level of abstraction
2. Having the program be free of low level errors in memory management

# Higher productivity of low level programming tasks via increasing the level of abstraction

Having a higher level of abstraction for low level programming tasks enables less code to do the same thing.

For example in Ada to build a ordered map containing a ordered map of line numbers to the numbered of occurrences on that line.



    -- wordcount-main.adb
    procedure Wordcount.Main is
       Tbl: Wordmap.Map;
    begin
       Build_Table(Tbl, Standard_Input);
       Report(Standard_Output, Tbl);
    end Wordcount.Main;

    -- wordcount.ads
    with Ada.Containers;
    with Ada.Containers.Ordered_Maps;
    use Ada.Containers;

    with Ada.Strings.Unbounded;
    use Ada.Strings.Unbounded;

    with Ada.Text_Io;
    use Ada.Text_Io;

    package Wordcount is
        package Submap is new Ordered_Maps(Natural, Natural, "<", "=");
        use Submap;
        package Wordmap is new Ordered_Maps(Unbounded_String, Submap.Map, "<", "=");
        use Wordmap;
        procedure Build_Table(Table: in out Wordmap.Map; File: in File_Type; Separators: in String);
        procedure Build_Table(Table: in out Wordmap.Map; File: in File_Type);
        procedure Report(File: in File_Type; Table: in Wordmap.Map);
    end Wordcount;

    -- wordcount.adb
    with Gnat.String_Split;
    with Ada.Strings;
    use Ada.Strings;
    with Ada.Characters.Latin_1;
    use Ada.Characters;

    package body Wordcount is
        package Natural_Io is new Integer_Io(Natural);
        package String_Split renames Gnat.String_Split;
        procedure Build_Table(Table: in out Wordmap.Map; File: in File_Type) is
        begin
            Build_Table(Table, File, " ");
        end Build_Table;
        procedure Build_Table(Table: in out Wordmap.Map; File: in File_Type; Separators: in String) is
            N: aliased Natural;
            Line, Word: Unbounded_String;
            Posn: Wordmap.Cursor;
            Updated: Boolean;
            Words: String_Split.Slice_Set;
            procedure Update_Subtable(Key: in Unbounded_String; Element: in out Submap.Map) is
                Posn: Submap.Cursor;
                Updated: Boolean;
            begin
                Submap.Insert(Element, N, Posn, Updated);
                Submap.Replace_Element(Element, Posn, Submap.Element(Element, N)+1);
            end Update_Subtable;
        begin
            N := 1;
            while not End_Of_File(File) loop
                Line := To_Unbounded_String(Get_Line(File));
                String_Split.Create(Words, To_String(Line), Separators);
                for I in 1..String_Split.Slice_Count(Words) loop
                    Word := To_Unbounded_String(String_Split.Slice(Words, I));
                    Wordmap.Insert(Table, Word, Posn, Updated);
                    Wordmap.Update_Element(Table, Posn, Update_Subtable'access);
                end loop;
                N := N+1;
            end loop;
        end Build_Table;
        procedure Report(File: in File_Type; Table: in Wordmap.Map) is
            Iter: Wordmap.Cursor;
            procedure Output_Pair(Position: in Submap.Cursor) is
                Cs, Ls: String(1..10);
            begin
                Natural_Io.Put(Ls, Submap.Key(Position));
                Natural_Io.Put(Cs, Submap.Element(Position));
                Put_Line(File, Latin_1.Ht & Ls & ": " & Cs);
            end Output_Pair;
            procedure Output_Word(Key: in Unbounded_String; Element: in Submap.Map) is
            begin
                Put_Line(File, To_String(Key));
                Submap.Iterate(Element, Output_Pair'Access);
            end Output_Word;
        begin
            Iter := Wordmap.First(Table);
            while Wordmap.Has_Element(Iter) loop
                Wordmap.Query_Element(Iter, Output_Word'Access);
                Wordmap.Next(Iter);
            end loop;
        end Report;
    end Wordcount;
        
In Java to do the same thing

    // wordcount/Wordcount.java
    package wordcount;
    import java.io.BufferedReader;
    import java.io.InputStreamReader;
    import java.io.IOException;
    import java.io.PrintStream;
    import java.util.Map;
    import java.util.TreeMap;

    public class Wordcount {
        public static void buildTable(TreeMap<String, TreeMap<Long, Long>> table, BufferedReader r) throws IOException {
            buildTable(table, r, " ");
        }
        public static void buildTable(TreeMap<String, TreeMap<Long, Long>> table, BufferedReader r, String separators) throws IOException {
            long i = 1;
            String line;
            while((line = r.readLine()) != null) {
                i++;
                for(String word : line.split(separators)) {
                    table.putIfAbsent(word, new TreeMap<Long, Long>());
                    TreeMap<Long, Long> subtable = table.get(word);
                    subtable.put(i, subtable.getOrDefault(i, 0L)+1);
                }
            }
        }
        public static void report(PrintStream w, TreeMap<String, TreeMap<Long, Long>> table) {
            for(Map.Entry<String, TreeMap<Long, Long>> e : table.entrySet()) {
                w.printf("%s:\n", e.getKey());
                for(Map.Entry<Long, Long> p : e.getValue().entrySet()) {
                    w.printf("\t%10d: %10d\n", p.getKey(), p.getValue());
                }
            }
        }
        public static void main(String[] args) throws IOException {
            BufferedReader stdin = new BufferedReader(new InputStreamReader(System.in));
            TreeMap<String, TreeMap<Long, Long>> table = new TreeMap<String, TreeMap<Long, Long>>();
            buildTable(table, stdin);
            report(System.out, table);
        }
    }

While the Rust code looks alot like the Java code in terms of conciseness of tokens, it has the performance of the Ada code. 

    // lib.rs
    use std::io;
    use std::io::prelude::*;
    use std::collections::BTreeMap;

    pub type SubTable = BTreeMap<u64, u64>;
    pub type Table = BTreeMap<String, SubTable>;

    /// `build_table_by` builds the table of words with a parametric seperator.
    pub fn build_table_by<R: BufRead, F: Fn(char) -> bool>(table: &mut Table, r: R, separators: F) -> io::Result<()> {
        for (line, i) in r.lines().zip(1..) {
            let line = line?;
            for word in line.split(&separators) {
                let mut e = table.entry(word.to_owned()).or_insert(SubTable::new());
                let mut se = e.entry(i).or_insert(0);
                *se += 1;
            }
        }
        Ok(())
    }

    /// `build_table` builds a table of words using `' '` as the separator.
    pub fn build_table<R: BufRead>(table: &mut Table, mut r: R) -> io::Result<()> {
        build_table_by(table, &mut r, |s| s == ' ')
    }

    /// `report` writes a report of the the words in a table to a `Write`.
    pub fn report<W: Write>(mut w: W, table: &Table) -> io::Result<()> {
        for (k, v) in table {
            writeln!(w, "{}:", k)?;
            for (l, c) in v {
                writeln!(w, "\t{:10}: {:10}", l, c)?;
            }
        }
        Ok(())
    }


    // bin/wordcount.rs
    extern crate wordcount;

    use std::io;

    fn main() {
        let mut table = wordcount::Table::new();
        let stdin = io::stdin();
        wordcount::build_table(&mut table, stdin.lock()).unwrap();
        let stdout = io::stdout();
        wordcount::report(stdout.lock(), &table).unwrap();
    }
