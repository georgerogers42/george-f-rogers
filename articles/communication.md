{"title": "How Can Communication Affect the Flow of Work in an Organization",
 "author": "George Rogers",
 "posted": "2018-07-16T10:50:00-0500",
 "slug": "communication" }

Incentive Structures are the form of communication that most impact the flow of work in an organization. This means that if you want to change the flow of work the best place to start is the incentive structures. To do this we must understand what are incentive structures.

# Law of Incentive
The first law of communication is the law of incentive which reads: “You will get what you pay for and ask for especially when it is not what you actually want.” This means that you must get the incentives and non-tangible rewards correct before fixing anything. How do we fix an organization with perverse incentive structures of both types? It takes 4 steps

1. Ask yourself and your organization what do we actually want.
2. How do we measure the success on these goals.
3. Use the correct measurements to set the incentive structure.
4. Actually do it.

Asking what your organization actually wants.
Ask these 3 questions when it comes to asking what your organization actually wants

1. What is profitable in the long haul?
2. What makes your employees happy?
3. What is profitable in the short haul?

## How do we measure these three things
Here is the first laws of statistics “Lies, Damned Lies, and Statistics”, and the second “Measurements measure what they measure no more no less.” For example: Measuring the quality of an aircraft design by weight of the finished plane, and the heavier the better. This is a comical example but I think everyone has seen examples of this in bad incentive structures.

How do we align the measurements of performance to actual performance? We ask those three questions again and also ask one additional question.

1. Does this measure what is profitable in the long haul?
2. Does this measure what makes your employees happy?
3. Does this measure what profitable in the short haul?
4. Is this measurement easy to cheat?

If it fails any of these four tests significantly then it should not be used.

# Examples of bad communication
Everyday communication and the relationships between employees and their equals, superiors, and inferiors set the incentive to communicate vital information about what is actually going on in the organization. For example in an organization with toxic leadership the leaders have no clue if anything is going badly because their subordinates will tell them everything is awesome. Now this has consequences for an organization because if the boss things everything is awesome and everything isn’t bad decision making and the death of the organization is inevitable. How do we avoid a toxic environment for bad news.

1. Encourage the belief that all news is good news, because what you don’t know will hurt you!
2. Discourage the promotion of toxic leaders up the hierarchy of the organization.
3. Demote or fire toxic leaders from the organization.

When you achieve these things you are well on the path to a healthy organization.
