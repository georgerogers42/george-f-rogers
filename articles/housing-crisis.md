{ "title": "How economic meltdowns affect small scale businesses", "author": "George Rogers", "posted": "2018-07-14T12:04:00-0500", "slug": "meltdowns"}

# The basic theory of business cycles
What is an economic meltdown: is it an aberration to the boom phase? Or is it caused by malinvestment during the boom phase?
The answer is that it is caused by malinvestment during the boom phase.
What is malinvestment you may ask.
***Malinvestment is the misallocation of funds during the boom phase caused by low interest rates***

## The Boom Phase
During the boom phase the market is flooded with cheap central bank money and the economy seems to be cruising along.
But instead it is sowing the seeds of its own demise.
This is because the market cannot support all investment that the low interest rates signal the market is ready for.
This leads to a panic when it the boom phase is finished
(we will cover that in the next section, but first we need to cover the rest of the mechanics of the boom phase).

## The Panic
During the panic phase of the business cycle a shock occurs that causes the exposure of the excess investment.
During this phase many businesses go under especially small and medium size businesses (and even more so younger entrepreneurial enterprises).
Because the access to credit is tightened in a shocking manner:
the failure of the businesses is a lot like the game musical chairs.
Which is an irrational way of deciding what investments are valid.

## The Aftermath
During the aftermath stage credit is still tight making it hard for small businesses to survive.
This is the recession.
During this step your business should have good cash on hand, little debt, and especially good cash flow. 
These three things are good to have always but especially when the money is tight.

Now it is a lot harder to start a new company when the amount of money to be lent is low and therefore the interest rates are high.
This discourages small business formation until the economy is allowed to get back on it's feet.
## How Government Policies Discourage Recovery, aka. Regulatory Overhang</h2>
Regulatory overhang is when uncertainties about the government regulatory environment discourage risk taking.
These regulatory policies were what made the Great Recession the Great Recession.
For example Dodd Frank made it a pain to lend to anyone because of all the record keeping requirements and strictures on who and how to lend.

## How This Especially Hurts Small Buisnesses
These regulations especially hurt small and mid size businesses for they do not have the lawyers and accountants to ensure compliance with the rules.
This means that the big businesses benefit for the most part because their smaller competitors are regulated out of existence.

Regulatory overhang effects also exasperate housing shortages in over regulated markets.

# How Bad Land Use Policy Causes Worse Economic Meltdowns Via Housing Shortages And Bubbles
Restrictions on housing supply create an incentive for malinvestment to go into housing speculation of the wrong sort (for example house flipping, land banking, and the like).
This makes the resulting panic and aftermath worse because the money went to inherently nonproductive or even antiproductive enterprises.

## The basics of the Urban Consolidation Racket
The basic racket goes like this, the regional authority sets up a green belt (or Urban Containment/Consolidation Boundary).
Now this causes the price of housing to go up because land owners do not have to compete with land just down the block (if they are at the urban fringe).
Now this creates a means of risk free antiproductive rentiering.

For instance a landlord can charge 3000 for a crappy apartment because there is no competition anywhere in that labor market.
Speculators can just sit on condos and houses and have them go up in value because they don't have any incentive to build.

## Regulatory Overhang and Housing Shortages
With Urban Containments when combined with equally deadly brother Zoning creates political risk and regulatory overhang.
Political risk causes the developer to build what the people say they want not what the people actually want.
This means that the investment is more likely to not be worth the hassle (even if it gets built at all) exasperating the housing shortage (and the resultant bad effects).
Also the builders are less likely to be small businesses.
For what small business is well connected politically.
But are going to be large regional builders, that are best able to handle the politics.
## How This Causes Worse Economic Meltdowns
This causes more harmful economic meltdowns because of the risk is held by Joe Homeowner.
Because he is not prepared nor should be expected to bear the risks of having a too big of a mortgage for a house he should be able to afford on a normal market.
## Effect on commercial land
Small businesses are harmed in a multitude of ways, for example commercial rents keep going up in a predictable manner but most small business owners are not
able to prepare for the rent hike.
Thereby going under once that rent hike occurs.
This further helps big companies because they can afford higher rents and can force out the smaller competitors.
The commercial landlords because of the lease lengths will hold out for more money than the market will bear thereby creating empty storefronts.
This is well under way in cities such as New York City and San Francisco.
### Effect on Wages and Costs
Fight for $15 is caused by the housing shortages in certain metro areas where 10 bucks cannot get you any apartment, anywhere in the labor market.
But this only hurts the people in the long run because a business only has to pay the people that work for it,
and only for the hours that the employee worked.
So to cut expenses the employer cuts as much labor out of the process.
So that the employees are out of a job, and are worse off then when they started.
## Policy prescriptions to eliminate housing shortages
We must overturn *Euclid v. Amber Reality* in the US constitution by asserting the right to build any use provided there are no deed restrictions
and that the current zoning maps act as deed restrictions that must be renewed every 10 years at the subdivision level.
Also there can be no ordinance based minimum lot size over 1 acre, and any that exist are not converted to the deed restrictions,
and any plots over 40 acres are completely unrestricted.
This is the only true solution to the housing crisis.
