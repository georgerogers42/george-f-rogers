{"title": "Democratic Socialism Is Responsible For The Housing Crisis", "author": "George Rogers", "posted": "2018-07-15T16:18:00-0500", "slug": "democratic-socialism-and-housing-crisis"}

<div class="img">
<img src="/static/img/Pruitt-igoe_collapse-series.jpg"><br>
<a href="https://en.wikipedia.org/wiki/Pruitt%E2%80%93Igoe#/media/File:Pruitt-igoe_collapse-series.jpg">Pruitt Igoe Demoliton</a>
</div>

Democratic socialism is responsible for the housing crisis. This is because zoning is a form of democratic socialism. But first to prove that point I must define what socialism is: Socialism is the control of property by either non-owners (that do not have legitimate leasehold), or illegitimate owners. What is zoning: Zoning is the control of the uses of private land by the government. Given these facts: Zoning is a form of socialism for the government cannot be a holder of private land so it by zoning is exercising control as a non-owner. Some cities (cough San Francisco cough), make the problem worse by having building permits that are not of right even when you have the proper zoning. In this case you can call the property markets in these cities almost fully socialized.

Now what are the effects of these policies on the supply of housing? It strangles supply because people do not want more housing next door or in the open space outside the city, hence urban growth boundaries. Special interests control the zoning meetings causing nothing to ever be built ever!

If the current problems are caused by lack of respect for private property and the natural right to build, what makes one think that going full communist (through eminent domain abuse that would make New London’s abuse of the system look like small potatoes) will solve? Does one expect the rich people to sit idly while the last remnants of their property are taken? Can one expect the productive to continue to be productive with their life’s assets stolen? If the rich stop them from doing so you will keep the status quo, but if they succeed in this program it will be worse.

How much worse, a neutral observer may ask. A lot worse: Ladies and Gentlemen the Bronx will Burn. History time and again shows that full communist urban development gives you Cabrini, Pruitt Igoe, Robert Tailor, or the city of Detroit proper; lest I name more. Does one want the same results from the same policies?
