{"title": "Yes Bernie, housing is a human right; but not in the way you think.", "posted": "2018-07-14T21:00:00-0500", "author": "George Rogers", "slug": "housing-is-a-human-right"}

<div class="img">
<img alt="Uptown Houston" src="https://upload.wikimedia.org/wikipedia/commons/thumb/d/da/Panorama_of_Uptown_Houston_from_Greenway_Plaza.jpg/1920px-Panorama_of_Uptown_Houston_from_Greenway_Plaza.jpg"><br>
<a href="https://commons.wikimedia.org/wiki/Houston#/media/File:Panorama_of_Uptown_Houston_from_Greenway_Plaza.jpg">Uptown Houston</a>
</div>

# The Right To Build

Housing is an important good that must be provided by oneself to live life, but in most of our cities the right to build is severely restricted so that the ability to afford housing is removed for a third of the population. Now why does this occur in the most left leaning cities, but not in more conservative cities? This is the case because left leaning cities have completely destroyed the natural right to build, and therefore have the problems attendant to the lack of housing. The people living on the street, or in cars; or in hood apartments causing excessive gentrification.

In Houston the only city and metro with no zoning, (95% of the metro is unzoned), which means the right to build is largely respected, with minimal hurdles to the construction of new housing, and commercial/industrial space. This allows much more of the population to be properly housed, if you can hold a job you can have at least a studio apartment. Now compare this to other cities such as New York where an apartment in Queens costs more than an luxury unit in Houston. For example a low end older place in Montrose in Houston costs under $850, while in New York City you can’t even find a place in da Bronx for that much.

[Here is a place in The Bronx for at least $1500,](https://www.zillow.com/homedetails/Walton-Ave-Bronx-NY-10452/2096268347_zpid/?fullpage=true)
[and this is a Place in Montrose Houston for less than $800.](https://www.zillow.com/homes/for_rent/2094615474_zpid/0-251218_price/0-1000_mp/29.776782,-95.372701,29.713662,-95.464969_rect/13_zm/)
Let us do some math, I’ll take someone that earns $10 * 60 * 52 from two jobs or $31,200. Let’s put that over 40 yielding $780.00. In New York the person would be out of luck because of the minimum wage hours will be harder to come by, thereby making it harder to earn $31,200; while in Houston the price of low end labor on the legal market is not artificially inflated thereby allowing the worker to work the hours necessary to afford his pad. In addition the place in the Bronx costs $1500.00 which is twice what the hypothetical low wage earner can afford. Now even taking account of the price of a car the low wage earner would be better off in Houston.
