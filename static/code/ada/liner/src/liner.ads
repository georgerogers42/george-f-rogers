with Ada.Strings.Unbounded;
use Ada.Strings.Unbounded;

package Liner is
   protected type Printer is
      entry Put_Line(Line : in Unbounded_String);
      entry Put_Line;
   private
      L : Unbounded_String := Null_Unbounded_String;
      C : Natural := 0;
   end Printer;
   procedure Reader(P : in out Printer);
   task type Ticker(P : access Printer) is
      entry Stop;
   end Ticker;
end Liner;
