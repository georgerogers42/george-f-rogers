with Ada.Text_Io;
use Ada.Text_Io;
with Ada.Strings;
use Ada.Strings;
with Ada.Characters.Latin_1;
use Ada.Characters;

package body Liner is
   procedure Reader(P: in out Printer) is
      Line : Unbounded_String;
   begin
      while not End_Of_File loop
         Line := To_Unbounded_String(Get_Line);
         P.Put_Line(Line);
      end loop;
   end Reader;
   procedure Output(L : in Unbounded_String; C : in out Natural) is
      S : Unbounded_String;
   begin
      S := To_Unbounded_String(Natural'Image(C)) & ": " & L;
      Trim(Source => S, Side => Both);
      Put_Line(To_String(S));
      C := C+1;
   end Output;
   protected body Printer is
      entry Put_Line(Line : in Unbounded_String) when True is
      begin
         C := 0;
         L := Line;
         Output(L, C);
      end Put_Line;
      entry Put_Line when True is
      begin
         Output(L, C);
      end Put_Line;
   end Printer;
   task body Ticker is
   begin
      loop
         select
            accept Stop;
            exit;
         or
            delay 0.5;
            P.Put_Line;
         end select;
      end loop;
   end Ticker;
end Liner;
