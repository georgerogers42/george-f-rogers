with System.Pool_Local;
use System.Pool_Local;
procedure Liner.Main is
   Pool : Unbounded_Reclaim_Pool;
   type Ticker_Access is access all Ticker;
   for Ticker_Access'Storage_pool use Pool;
   P : aliased Printer;
   T : Ticker_Access := new Ticker(P'Access);
begin
   Reader(P);
   T.Stop;
end Liner.Main;
