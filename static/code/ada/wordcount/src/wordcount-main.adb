procedure Wordcount.Main is
   Tbl: Wordmap.Map;
begin
   Build_Table(Tbl, Standard_Input);
   Report(Standard_Output, Tbl);
end Wordcount.Main;
