with Gnat.String_Split;
with Ada.Strings;
use Ada.Strings;
with Ada.Characters.Latin_1;
use Ada.Characters;

package body Wordcount is
    package Natural_Io is new Integer_Io(Natural);
    package String_Split renames Gnat.String_Split;
    procedure Build_Table(Table: in out Wordmap.Map; File: in File_Type) is
    begin
        Build_Table(Table, File, " ");
    end Build_Table;
    procedure Build_Table(Table: in out Wordmap.Map; File: in File_Type; Separators: in String) is
        N: aliased Natural;
        Line, Word: Unbounded_String;
        Posn: Wordmap.Cursor;
        Updated: Boolean;
        Words: String_Split.Slice_Set;
        procedure Update_Subtable(Key: in Unbounded_String; Element: in out Submap.Map) is
            Posn: Submap.Cursor;
            Updated: Boolean;
        begin
            Submap.Insert(Element, N, Posn, Updated);
            Submap.Replace_Element(Element, Posn, Submap.Element(Element, N)+1);
        end Update_Subtable;
    begin
        N := 1;
        while not End_Of_File(File) loop
            Line := To_Unbounded_String(Get_Line(File));
            String_Split.Create(Words, To_String(Line), Separators);
            for I in 1..String_Split.Slice_Count(Words) loop
                Word := To_Unbounded_String(String_Split.Slice(Words, I));
                Wordmap.Insert(Table, Word, Posn, Updated);
                Wordmap.Update_Element(Table, Posn, Update_Subtable'access);
            end loop;
            N := N+1;
        end loop;
    end Build_Table;
    procedure Report(File: in File_Type; Table: in Wordmap.Map) is
        Iter: Wordmap.Cursor;
        procedure Output_Pair(Position: in Submap.Cursor) is
            Cs, Ls: String(1..10);
        begin
            Natural_Io.Put(Ls, Submap.Key(Position));
            Natural_Io.Put(Cs, Submap.Element(Position));
            Put_Line(File, Latin_1.Ht & Ls & ": " & Cs);
        end Output_Pair;
        procedure Output_Word(Key: in Unbounded_String; Element: in Submap.Map) is
        begin
            Put_Line(File, To_String(Key));
            Submap.Iterate(Element, Output_Pair'Access);
        end Output_Word;
    begin
        Iter := Wordmap.First(Table);
        while Wordmap.Has_Element(Iter) loop
            Wordmap.Query_Element(Iter, Output_Word'Access);
            Wordmap.Next(Iter);
        end loop;
    end Report;
end Wordcount;
