with Ada.Containers;
with Ada.Containers.Ordered_Maps;
use Ada.Containers;

with Ada.Strings.Unbounded;
use Ada.Strings.Unbounded;

with Ada.Text_Io;
use Ada.Text_Io;

package Wordcount is
    package Submap is new Ordered_Maps(Natural, Natural, "<", "=");
    use Submap;
    package Wordmap is new Ordered_Maps(Unbounded_String, Submap.Map, "<", "=");
    use Wordmap;
    procedure Build_Table(Table: in out Wordmap.Map; File: in File_Type; Separators: in String);
    procedure Build_Table(Table: in out Wordmap.Map; File: in File_Type);
    procedure Report(File: in File_Type; Table: in Wordmap.Map);
end Wordcount;
