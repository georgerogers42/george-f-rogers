#[macro_use]
extern crate chan;

use std::thread::{JoinHandle, spawn};
use std::io::prelude::*;
use std::io;
use std::sync::{Arc, Mutex};
use std::time::Duration;
use chan::{Sender, Receiver};

struct Printer {
    l: String,
    c: usize,
}

impl Printer {
    fn new() -> Printer {
        Printer { l: "".to_owned(), c: 0 }
    }
    fn outdate(&mut self) {
        let s = format!("{}: {}", self.c, self.l);
        println!("{}", s.trim());
        self.c += 1;
    }
    fn output(&mut self, l: String) {
        self.c = 0;
        self.l = l;
        self.outdate();
    }
}

fn ticker_loop(p: &Mutex<Printer>, sr: Receiver<()>) {
    loop {
        let tr = chan::after(Duration::from_millis(500));
        chan_select! {
            tr.recv() => {
                let mut p = p.lock().unwrap();
                p.outdate();
            }, sr.recv() => {
                return;
            }
        }
    }
}

fn ticker(p: Arc<Mutex<Printer>>) -> (Sender<()>, JoinHandle<()>) {
    let (ts, tr) = chan::sync(0);
    let jh = spawn(move || ticker_loop(&p, tr));
    (ts, jh)
}

fn reader(p: &Mutex<Printer>) {
    let stdin = io::stdin();
    for line in stdin.lock().lines() {
        let line = line.unwrap();
        let mut p = p.lock().unwrap();
        p.output(line);
    }
}

fn main() {
    let p = Arc::new(Mutex::new(Printer::new()));
    let (ts, jh) = ticker(p.clone());
    reader(&p);
    ts.send(());
    jh.join().unwrap();
}
