extern crate wordcount;

use std::io;

fn main() {
    let mut table = wordcount::Table::new();
    let stdin = io::stdin();
    wordcount::build_table(&mut table, stdin.lock()).unwrap();
    let stdout = io::stdout();
    wordcount::report(stdout.lock(), &table).unwrap();
}
