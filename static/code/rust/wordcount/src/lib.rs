use std::io;
use std::io::prelude::*;
use std::collections::BTreeMap;

pub type SubTable = BTreeMap<u64, u64>;
pub type Table = BTreeMap<String, SubTable>;


/// `build_table_by` builds the table of words with a parametric seperator.
pub fn build_table_by<R: BufRead, F: Fn(char) -> bool>(table: &mut Table, r: R, separators: F) -> io::Result<()> {
    for (line, i) in r.lines().zip(1..) {
        let line = line?;
        for word in line.split(&separators) {
            let mut e = table.entry(word.to_owned()).or_insert(SubTable::new());
            e.entry(i).and_modify(|se| *se += 1).or_insert(1);
        }
    }
    Ok(())
}

/// `build_table` builds a table of words using `' '` as the separator.
pub fn build_table<R: BufRead>(table: &mut Table, mut r: R) -> io::Result<()> {
    build_table_by(table, &mut r, |s| s == ' ')
}

/// `report` writes a report of the the words in a table to a `Write`.
pub fn report<W: Write>(mut w: W, table: &Table) -> io::Result<()> {
    for (k, v) in table {
        writeln!(w, "{}:", k)?;
        for (l, c) in v {
            writeln!(w, "\t{:10}: {:10}", l, c)?;
        }
    }
    Ok(())
}
