package liner;

public class Printer {
    private String line = "";
    private long count = 0;
    private boolean done = false;
    public boolean isDone() { return done; }
    public void setDone(boolean done) { this.done = done; }
    public void output() {
        System.out.printf("%d: %s\n", count++, line);
    }
    public void output(String l) {
        count = 0;
        line = l;
        output();
    }
}
