package liner;
import java.io.BufferedReader;
import java.io.IOException;

public class ReadTask implements Runnable {
    private Printer printer;
    private BufferedReader input;
    public ReadTask(Printer printer, BufferedReader input) {
        this.printer = printer;
        this.input = input;
    }
    public void run() {
        try {
            String line;
            while((line = input.readLine()) != null) {
                synchronized(printer) {
                    printer.output(line);
                }
            }
            synchronized(printer) {
                printer.setDone(true);
            }
        } catch(IOException e) {
            throw new RuntimeException(e);
        }
    }
}
