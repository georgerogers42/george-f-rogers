package liner;
import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Liner {
    public static void main(String[] args) throws InterruptedException {
        BufferedReader stdin = new BufferedReader(new InputStreamReader(System.in));
        Printer p = new Printer();
        Thread r = new Thread(new ReadTask(p, stdin));
        r.start();
        ticker(p);
    }
    public static void ticker(Printer p) throws InterruptedException {
        for(;;) {
            Thread.sleep(500);
            synchronized(p) {
                if(p.isDone()) {
                    break;
                }
                p.output();
            }
        }
    }
}
