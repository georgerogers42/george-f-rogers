package wordcount;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Map;
import java.util.TreeMap;

public class Wordcount {
    public static void buildTable(TreeMap<String, TreeMap<Long, Long>> table, BufferedReader r) throws IOException {
        buildTable(table, r, " ");
    }
    public static void buildTable(TreeMap<String, TreeMap<Long, Long>> table, BufferedReader r, String separators) throws IOException {
        long i = 1;
        String line;
        while((line = r.readLine()) != null) {
            for(String word : line.split(separators)) {
                table.putIfAbsent(word, new TreeMap<Long, Long>());
                TreeMap<Long, Long> subtable = table.get(word);
                subtable.put(i, subtable.getOrDefault(i, 0L)+1);
            }
            i++;
        }
    }
    public static void report(PrintStream w, TreeMap<String, TreeMap<Long, Long>> table) {
        for(Map.Entry<String, TreeMap<Long, Long>> e : table.entrySet()) {
            w.printf("%s:\n", e.getKey());
            for(Map.Entry<Long, Long> p : e.getValue().entrySet()) {
                w.printf("\t%10d: %10d\n", p.getKey(), p.getValue());
            }
        }
    }
    public static void main(String[] args) throws IOException {
        BufferedReader stdin = new BufferedReader(new InputStreamReader(System.in));
        TreeMap<String, TreeMap<Long, Long>> table = new TreeMap<String, TreeMap<Long, Long>>();
        buildTable(table, stdin);
        report(System.out, table);
    }
}
